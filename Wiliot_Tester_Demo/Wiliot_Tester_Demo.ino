#include <avr/wdt.h>
#define gpio_end_of_charge 2 //End of Charge response [OUT]
#define gpio_pass0 3 //Pass0 Response  [OUT]
#define gpio_new_batch 4 //New Batch Input [IN]
#define gpio_start_test 5 //Start Test Input [IN]
#define gpio_fail0 6 //Fail0 Response  [OUT]
#define gpio_pass1 7 //Pass1 Response  [OUT]
#define gpio_fail1 8 //Fail1 Response  [OUT]
#define gpio_pass2 9 //Pass2 Response  [OUT]
#define gpio_fail2 10 //Fail2 Response  [OUT]

#define gpioLED 13
String label_idn = "*IDN?";
String label2 = "SER?";
String label_end_of_charge = "end of charge";
String label_good = "good";
String label_bad = "bad";
String label_new_batch = "new batch";
String label_start = "start";

String label_reset = "RESET";
String data ;
String idn = "Williot Tester_Demo";
int serial = 1;
bool ran;
void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
   wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}

void setup() {
    Serial.begin(1000000);  // Start the Serial monitor with speed of 9600 Bauds
    Serial.setTimeout(10);
    Serial.println(idn);
    
//    delay(5); 
    pinMode(gpio_end_of_charge,OUTPUT);
    pinMode(gpio_pass0,OUTPUT);
    pinMode(gpio_fail0,OUTPUT);
    pinMode(gpio_pass1,OUTPUT);
    pinMode(gpio_fail1,OUTPUT);
    pinMode(gpio_pass2,OUTPUT);
    pinMode(gpio_fail2,OUTPUT);
    pinMode(gpioLED,OUTPUT);
    digitalWrite(gpio_end_of_charge,HIGH);
    digitalWrite(gpio_pass0,HIGH);
    digitalWrite(gpio_fail0,HIGH);
    digitalWrite(gpioLED,LOW);
    delay(5); 
    pinMode(gpio_new_batch, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(gpio_new_batch), new_batch, RISING);
    pinMode(gpio_start_test, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(gpio_start_test), start, RISING);
    Serial.flush();
}

void pulseOut(int pin, int us)
{
   digitalWrite(pin, LOW);
  // us = max(us - 20, 1);
   digitalWrite(gpioLED, LOW);
   delay(us);
   digitalWrite(pin, HIGH);
   digitalWrite(gpioLED, HIGH);
}  

void new_batch(){
   detachInterrupt(digitalPinToInterrupt(gpio_new_batch));
   //todo - should I add flush here? is there a risk the line will be written twice? -maybe
   //the way to solve it - use switch case on global var (state in Dima's code)
   Serial.println(label_new_batch);
   attachInterrupt(digitalPinToInterrupt(gpio_new_batch), new_batch, RISING);
  }
void start(){
  detachInterrupt(digitalPinToInterrupt(gpio_start_test));
  Serial.println(label_start);
  attachInterrupt(digitalPinToInterrupt(gpio_start_test), start, RISING);
  }

void loop() {
  if (Serial.available()>0)  { // Check if values are available in the Serial Buffer      
      data = Serial.readString();  
      if (data==label_idn){
        Serial.println(idn);
        }
      
      else if (data==label_end_of_charge){
        digitalWrite(gpio_start_test,LOW);
        // fo debug - digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
       
      else if (data==label_reset){
         softwareReset(WDTO_60MS); //call reset 
        }
      else if (data.substring(0,13)==label_end_of_charge){ 
         pulseOut(gpio_end_of_charge,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,4)==label_good){
         if (data.substring(4).toInt()==0){
            pulseOut(gpio_pass0, 2);
            Serial.println("Completed Successfully");   
         }
         else if (data.substring(4).toInt()==1){
            pulseOut(gpio_pass1, 2);
            Serial.println("Completed Successfully");
         }
         else if (data.substring(4).toInt()==2){
            pulseOut(gpio_pass2, 2);
            Serial.println("Completed Successfully");
         }
        }
      else if (data.substring(0,3)==label_bad){ 
         if (data.substring(3).toInt()==0){
            pulseOut(gpio_fail0, 2);
            Serial.println("Completed Successfully");
         }
         else if (data.substring(3).toInt()==1){
            pulseOut(gpio_fail1, 2);
            Serial.println("Completed Successfully");
         }
         else if (data.substring(3).toInt()==2){
            pulseOut(gpio_fail2, 2);
            Serial.println("Completed Successfully");
         }
        }  
      else {
         Serial.println(data);
         Serial.println("Not IDENTIEFIED");
      }
  }
   
}
