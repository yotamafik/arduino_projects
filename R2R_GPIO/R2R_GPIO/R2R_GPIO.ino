#include <avr/wdt.h>
#define gpio1PF 12
#define gpio2PF 2
#define gpio3PF 3
#define gpio4PF 4
#define gpioLED 13
String label1 = "*IDN?";
String label2 = "SER?";
String label3 = "GPIO1_ON";
String label4 = "GPIO1_OFF";
String label5 = "GPIO1_PULSE";
String label6 = "GPIO2_ON";
String label7 = "GPIO2_OFF";
String label8 = "GPIO2_PULSE";

String label9 = "GPIO3_ON";
String label12 = "GPIO3_OFF";
String label13 = "GPIO3_PULSE";
String label14 = "GPIO4_ON";
String label15 = "GPIO4_OFF";
String label16 = "GPIO4_PULSE";

String label10 = "STATE";
String label11 = "RESET";
String data ;
String idn = "Williot R2R GPIO 0.2";
int serial = 1;

void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
   wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}

void setup() {
    Serial.begin(1000000);  // Start the Serial monitor with speed of 9600 Bauds
    Serial.setTimeout(10);
    Serial.println(idn);
    
//    delay(5); 
    pinMode(gpio1PF,OUTPUT);
    pinMode(gpio2PF,OUTPUT);
    pinMode(gpio3PF,OUTPUT);
    pinMode(gpio4PF,OUTPUT);
    pinMode(gpioLED,OUTPUT);
    digitalWrite(gpio1PF,LOW);
    digitalWrite(gpio2PF,LOW);
    digitalWrite(gpio3PF,LOW);
    digitalWrite(gpio4PF,LOW);
    digitalWrite(gpioLED,LOW);
    Serial.flush();
}

void pulseOut(int pin, int us)
{
   digitalWrite(pin, HIGH);
   digitalWrite(13, HIGH);
  // us = max(us - 20, 1);
   delay(us);
   digitalWrite(pin, LOW);
   digitalWrite(13, LOW);
}  

void loop() {
 while (Serial.available()>0)  { // Check if values are available in the Serial Buffer
      data = Serial.readString();  
//      Read the incoming data & store into data
      if (data==label1){
        Serial.println(idn);
        }
      else if (data==label2){
        Serial.println(serial);
        }

      else if (data==label3){
        digitalWrite(gpio1PF,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label4){
        digitalWrite(gpio1PF,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label6){
        digitalWrite(gpio2PF,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label7){
        digitalWrite(gpio2PF,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label9){
        digitalWrite(gpio3PF,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label12){
        digitalWrite(gpio3PF,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label14){
        digitalWrite(gpio4PF,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label15){
        digitalWrite(gpio4PF,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }

        
      else if (data==label11){
         softwareReset(WDTO_60MS); //call reset 
        }
      else if (data.substring(0,11)==label5){ 
         pulseOut(gpio1PF,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label8){ 
         pulseOut(gpio2PF,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label13){ 
         pulseOut(gpio3PF,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label16){ 
         pulseOut(gpio4PF,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }

        
      else {
         Serial.println(data);
         Serial.println("Not IDENTIEFIED");
      }
  }
   
}
